#include <stdio.h>
#include <string.h>
#include "msg_codec.h"
#include "io_defs.h"

#define WATCHDOG_MESSAGE     ":wd:"
#define CLOSE_MESSAGE        ":cl"
#define EXIT_MESSAGE         ":ex:"

/**
 * Test commands against receipts
 */
boolean_t command_test(ClientMessage_t command, char *received) {
    boolean_t    match_flag = FALSE;
    char         temp_buffer[CLIENT_MESSAGE_MAXLEN] = {'\0'};
    unsigned64_t cmd_length = MC_Encode(command, temp_buffer, CLIENT_MESSAGE_MAXLEN);

    if (strncmp(temp_buffer, received, cmd_length) == 0) {
        match_flag = TRUE;
    }
    return match_flag;
}

/**
 * Decode incoming
 */
ClientMessage_t MC_Decode(char *message, unsigned64_t length) {
    ClientMessage_t message_type = CM_Normal;

    if ((length == 0) || (command_test(CM_Close, message) == TRUE)) {
        message_type = CM_Close;
    } else if (command_test(CM_Watchdog, message) == TRUE) {
        message_type = CM_Watchdog;
    } else if (command_test(CM_Exit, message) == TRUE) {
        message_type = CM_Exit;
    }

    return message_type;
}

/**
 * Encode outgoing
 */
unsigned64_t MC_Encode(ClientMessage_t messageType, char *buffer, unsigned64_t maxLength) {
    unsigned64_t message_length = 0;

    switch(messageType) {
        case CM_Normal:
            message_length = strlen(buffer) - 1;
            break;
        case CM_Close:
            message_length = snprintf(buffer, maxLength, CLOSE_MESSAGE) -1;
            break;
        case CM_Exit:
            message_length = snprintf(buffer, maxLength, EXIT_MESSAGE) - 1;
            break;
        case CM_Watchdog:
            message_length = snprintf(buffer, maxLength, WATCHDOG_MESSAGE) - 1;
            break;
    }
    return message_length;
}
