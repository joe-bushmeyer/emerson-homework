/**
 * We'll define some of the basic client/server I/O parameters here
 */
#ifndef __IO_DEFS_H
#define __IO_DEFS_H

// Server will listen on specified port
#define LISTENING_PORT          5000
// Client messages will not exceed this maximum length
#define CLIENT_MESSAGE_MAXLEN   1024

// Listener will block for specified duration
#define LISTENER_PEND_SECONDS   1

// Maximum number of clients to handle
#define MAX_CLIENTS             256

// Amount of idle time before server closes socket
#define SOCKET_TIMEOUT_SECONDS  30

// Max client messages
#define MAX_CLIENT_MESSAGES     10

#endif
