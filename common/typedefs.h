/**
 * We'll define some of the basic client/server I/O parameters here
 */
#ifndef __TYPEDEFS_H
#define __TYPEDEFS_H

#include <stdbool.h>

typedef          int signed64_t;
typedef unsigned int unsigned64_t;
typedef bool         boolean_t;

#ifndef TRUE
#define TRUE         1
#endif
#ifndef FALSE
#define FALSE        0
#endif

#ifndef NULL
#define NULL ((void*)0)
#endif

/**
 * Enumerated listener results
 */
typedef enum ListenResult_tag {
    ListenResult_Error     = -1,
    ListenResult_Timeout   = 0,
    ListenResult_Available = 1,
} ListenResult_t;

/**
 * Enumerated client states
 */
typedef enum ConnectionState_tag {
    CS_Disconnected = 0,
    CS_Connected    = 1,
    CS_CloseNow     = 2,
} ConnectionState_t;

/**
 * Used for tracking the current state
 * of a client.
 */
typedef struct ClientState_tag {
    signed64_t        id;
    ConnectionState_t status;
    unsigned64_t      messsageCount;
    unsigned64_t      secondsSinceLast;
} ClientState_t;

/**
 * Used to distinguish between client
 * message types.
 */
typedef enum ClientMessage_tag {
    CM_Exit     = -1,
    CM_Close    = 0,
    CM_Normal   = 1,
    CM_Watchdog = 2,
} ClientMessage_t;

#endif
