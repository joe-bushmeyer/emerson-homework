#ifndef __MSG_CODEC_H
#define __MSG_CODEC_H

#include "typedefs.h"

/**
 * Decode incoming
 */
ClientMessage_t MC_Decode(char *message, unsigned64_t length);
/**
 * Encode outgoing
 */
unsigned64_t MC_Encode(ClientMessage_t messageType, char *buffer, unsigned64_t maxLength);

#endif
