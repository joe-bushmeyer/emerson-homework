#ifndef __CFG_MAIN_H
#define __CFG_MAIN_H
#include "../common/io_defs.h"

#define __UBUNTU_PLATFORM

/** Start Ubuntu platform client **/
#ifdef __UBUNTU_PLATFORM
/* Include the Ubuntu client functions */
#include "platforms/ubuntu/client.h"

#define CONNECT(ADDR)  \
    UBU_Connect(ADDR, LISTENING_PORT)

#define SEND(MESSAGE)   \
    UBU_Send(MESSAGE)

#define PLATFORM_EXIT() \
    exit(EXIT_FAILURE)
#endif
/** End Ubuntu platform client **/

#endif