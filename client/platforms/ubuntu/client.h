#ifndef __CLIENT_H
#define __CLIENT_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include "../../../common/typedefs.h"

/**
 * Setup connection to server.
 * Return true on success.
 */
boolean_t UBU_Connect(char *octectString, signed64_t port);
/**
 * Send the message through connection.
 * Return TRUE on success.
 */
boolean_t UBU_Send(char *message);
#endif
