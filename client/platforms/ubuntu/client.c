#include "client.h"

#define SOCKET_NOT_CONNECTED (-1)

static int SocketDescription = SOCKET_NOT_CONNECTED;

/**
 * Setup connection to server.
 * Return true on success.
 */
boolean_t UBU_Connect(char *octectString, signed64_t port) {
	struct sockaddr_in  server;
    boolean_t           success_flag = TRUE;

	//Create socket
	SocketDescription = socket(AF_INET , SOCK_STREAM , 0);
	if (SocketDescription == -1)
	{
		printf("Could not create socket");
		succeeded = FALSE;
		return FALSE;
	}
		
	server.sin_addr.s_addr = inet_addr(octectString);
	server.sin_family = AF_INET;
	server.sin_port = htons((uint16_t)port);

	//Connect to remote server
    printf("Attempting connection to %s on port %d ...", octectString, port);
	if (connect(SocketDescription, (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		printf("\nConnection error\n");
        success_flag = FALSE;
		return FALSE;
	}

	printf("\nConnection succeeded.\n");
	return success_flag;
}

/**
 * Send the message through connection.
 * Return TRUE on success.
 */
boolean_t UBU_Send(char *message) {
    boolean_t success_flag = FALSE;

    if (SocketDescription != SOCKET_NOT_CONNECTED) {
        if (send(SocketDescription, message, strlen(message), 0) >= 0) {
            success_flag = TRUE;
        }
    }
    return success_flag;
}
