#include <stdio.h>
#include "../common/typedefs.h"
#include "../common/msg_codec.h"
#include "cfg_main.h"

#define ENTER_SERVER_ADDRESS_TEXT                   \
    "\n\tEnter server address: "

#define MAIN_MENU_TEXT                              \
    "\n********** Simple Client Menu **********\n"  \
    "\t1. Send Message\n"                           \
    "\t2. Send Watchdog/Heartbeat\n"                \
    "\t3. Send \"Close\" Command\n"                 \
    "\t4. Send \"Exit\" Command\n\n"                \
    "\t>  "

#define MESSAGE_SUBMENU_TEXT                        \
    "\n\tEnter message as string: "

#define ENCODE(x)    \
    MC_Encode(x, message_buffer, CLIENT_MESSAGE_MAXLEN)

static BOOLEAN get_message(char *buffer, int length) {

    for (int i = 0; i <= length; i++) {
        if (i == length) {
            return FALSE;
        }
        char current = (char)getchar();
        if (current < 0) {
            break;
        }
        buffer[i] = current;
    }
    return TRUE;
}

int main(void) {
    int cmd;
    char message_buffer[CLIENT_MESSAGE_MAXLEN] = {'\0'};
    char address[64] = {'\0'};

    /**
     * Get address of server
     */
    printf(ENTER_SERVER_ADDRESS_TEXT);
    scanf("%s", address);
    if (CONNECT(address) == FALSE) {
        printf("Error connecting to address %s", address);
        PLATFORM_EXIT();
    }

    while (1) {
        /**
         * Loop on the main menu until
         * we select 3 or 4.
         */
        printf(MAIN_MENU_TEXT);
        scanf("%d", &cmd);
        printf("\n\tYou've selected: %d\n", cmd);

        switch (cmd) {
            case 1:
            // Custom message here
                printf(MESSAGE_SUBMENU_TEXT);
                char temp;
                // scanf("%c", &temp);
                // scanf("%[^\n]", message_buffer);
                if (get_message(message_buffer, CLIENT_MESSAGE_MAXLEN)) {
                    printf("\n\tYou wrote: %s\n", message_buffer);
                    SEND(message_buffer);
                }
                else {
                    printf("\n\tERROR: Your input was too long!\n");
                }
                break;
            case 2:
            // WD/HB here
                ENCODE(CM_Watchdog);
                printf("\n\tSending Watchdog/Heartbeat (%s) ...\n", message_buffer);
                SEND(message_buffer);
                break;
            // Close
            case 3:
                ENCODE(CM_Close);
                printf("\n\tSending \"Close\" (%s) ...\n", message_buffer);
                SEND(message_buffer);
                PLATFORM_EXIT();
                break;
            // Exit
            case 4:
                ENCODE(CM_Exit);
                printf("\n\tSending \"Exit\" (%s) ...\n", message_buffer);
                SEND(message_buffer);
                PLATFORM_EXIT();
                break;
            default:
                break;
        }
    }
}
