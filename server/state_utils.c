#include "state_utils.h"
#include <stdio.h>

/**
 * Initialize array of states to show nothing connected (yet)
 */
void SU_InitClientStates(ClientState_t *states, unsigned64_t numClients) {
    for (unsigned64_t i = 0; i < numClients; i++) {
        states[i].status = CS_Disconnected;
    }
}

/**
 * Find an empty "state" variable and start tracking a "new" client connection
 */
boolean_t SU_InitConnectedClientState(signed64_t id, ClientState_t *states, unsigned64_t numClients) {
    boolean_t success_flag = FALSE;

    for (unsigned64_t i = 0; i < numClients; i++) {
        if (states[i].status == CS_Disconnected) {
            states[i].id = id;
            states[i].messsageCount = 0;
            states[i].secondsSinceLast = 0;
            states[i].status = CS_Connected;
            success_flag = TRUE;
            break;
        }
    }
    return success_flag;
}

/**
 * Update timeout trackers on connected clients
 */
void SU_UpdateTimeouts(ClientState_t *states, unsigned64_t numClients) {
    for (unsigned64_t i = 0; i < numClients; i++) {
        if (states[i].status == CS_Connected) {
            states[i].secondsSinceLast++;
            if (states[i].secondsSinceLast >= SOCKET_TIMEOUT_SECONDS) {
                states[i].status = CS_CloseNow;
            }
        }
    }
}

/**
 * Update message count for connected client with matching id
 */
void SU_UpdateMessageCount(signed64_t id, ClientState_t *states, unsigned64_t numClients) {
    for (unsigned64_t i = 0; i < numClients; i++) {
        if ((states[i].status == CS_Connected) &&
            (states[i].id == id)) {
            states[i].messsageCount++;
            printf("Client %d: #%d\n", states[i].id, states[i].messsageCount);
            if (states[i].messsageCount >= MAX_CLIENT_MESSAGES) {
                states[i].status = CS_CloseNow;
            }
        }
    }
}

/**
 * Mark client connection for immediate close
 */
void SU_MarkForClosure(signed64_t id, ClientState_t *states, unsigned64_t numClients) {
    for (unsigned64_t i = 0; i < numClients; i++) {
        if ((states[i].status == CS_Connected) &&
            (states[i].id == id)) {
            states[i].status = CS_CloseNow;
        }
    }
}

/**
 * Kick the dog on receipt of wd message
 */
void SU_KickTheDog(signed64_t id, ClientState_t *states, unsigned64_t numClients) {
    for (unsigned64_t i = 0; i < numClients; i++) {
        if ((states[i].status == CS_Connected) &&
            (states[i].id == id)) {
            states[i].secondsSinceLast = 0;
            break;
        }
    }
}
