#ifndef __CFG_MAIN_H
#define __CFG_MAIN_H

#define __UBUNTU_PLATFORM

/** Start Ubuntu platform server **/
#ifdef __UBUNTU_PLATFORM
/* Include the Ubuntu server functions */
#include "platforms/ubuntu/server.h"

/* Unecessary for now, but maybe later */
#define SERVER_INIT()

#define BLOCKING_LISTENER(LISTEN_PORT, TIMEOUT_IN_SEC) \
    UBU_BlockingListener(LISTEN_PORT, TIMEOUT_IN_SEC)

#define PLATFORM_EXIT() \
    exit(EXIT_FAILURE)

#define INPUT_PENDING(SOCKET_ID)    \
    UBU_IsInputPending(SOCKET_ID)

/* Socket count for "pending" test loop */
#define SOCKETS_MAX_COUNT FD_SETSIZE

#define NEW_CONNECTION_TEST(SOCKET_ID)  \
    UBU_IsNewConnection(SOCKET_ID)

#define ACCEPT_CONNECTION_REQUEST(SOCKET_ID) \
    UBU_AcceptConnectRequest(SOCKET_ID)

#define READ_FROM_CLIENT(SOCKET_ID, BUFFER, MAXLENGTH)  \
    UBU_ReadFromClient(SOCKET_ID, BUFFER, MAXLENGTH)

#define CLOSE_SOCKET_CONNECTION(SOCKET_ID)  \
    UBU_CloseClientConnection(SOCKET_ID)

#endif
/** End Ubuntu platform server **/

#endif