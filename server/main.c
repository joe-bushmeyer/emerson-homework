#include "cfg_main.h"
#include "state_utils.h"
#include "../common/io_defs.h"
#include "../common/typedefs.h"
#include "../common/msg_codec.h"

/**
 * Check all possible connections to see if any are flaggeed
 * for closure.
 */
static void close_flagged_connections(ClientState_t *states) {
    for (unsigned64_t i = 0; i < MAX_CLIENTS; i++) {
        if (states[i].status == CS_CloseNow) {
            CLOSE_SOCKET_CONNECTION(states[i].id);
            states[i].status = CS_Disconnected;
        }
    }
}

/**
 * Get the client message and determine whether its a "normal"
 * message or a "special" command.
 */
static ClientMessage_t read_from_client(signed64_t fd) {
    char         buffer[CLIENT_MESSAGE_MAXLEN] = {'\0'};

    unsigned64_t response_length =
        READ_FROM_CLIENT(fd, buffer, CLIENT_MESSAGE_MAXLEN);
    printf("Client says: %s\n", buffer);
    
    return MC_Decode(buffer, response_length);
}

int main(void) {
    ListenResult_t result  = ListenResult_Timeout;
    ClientState_t  client_states[MAX_CLIENTS];

    // Perform startup initialization(s)
    SU_InitClientStates(client_states, MAX_CLIENTS);
    SERVER_INIT();

    printf("Listening on port %d\n", LISTENING_PORT);

    while (1) {
        // Listen for messages on the specified port
        result = BLOCKING_LISTENER(LISTENING_PORT, LISTENER_PEND_SECONDS);

        switch(result) {
            default:
            case ListenResult_Timeout:
                /* Mark the passage of time on open connections */
                SU_UpdateTimeouts(client_states, MAX_CLIENTS);
                /* Close connections that timed out immediately */
                close_flagged_connections(client_states);
                continue;   /* <--- start listening again without further processing */
            case ListenResult_Error:
                /* Handle an unrecoverable error */
                PLATFORM_EXIT();
                break;
            case ListenResult_Available:
                /* continue processing below */
                break;
        }

        /**
         * Begin handling new connections and new messages here.
         */
        for (unsigned64_t i = 0; i < SOCKETS_MAX_COUNT; ++i) {
            if (INPUT_PENDING(i) == TRUE) {
                if (NEW_CONNECTION_TEST(i) == TRUE) {
                    /* Init connection stats */
                    signed64_t new_id = ACCEPT_CONNECTION_REQUEST(i);
                    printf("New connection, %d.\n", new_id);
                    if (SU_InitConnectedClientState(new_id, client_states, MAX_CLIENTS) == FALSE) {
                        // We're maxed out here.
                        CLOSE_SOCKET_CONNECTION(new_id);
                    }
                } else {
                    /* Check to see what type of message was received */
                    switch (read_from_client(i)) {
                        case CM_Watchdog:
                            printf("WD received from %d.\n", i);
                            SU_KickTheDog(i, client_states, MAX_CLIENTS);
                            break;
                        case CM_Close:
                            printf("Close received from %d.\n", i);
                            SU_MarkForClosure(i, client_states, MAX_CLIENTS);
                            break;
                        case CM_Exit:
                            printf("Exit received from %d.\n", i);
                            PLATFORM_EXIT();
                            break;
                        default:
                        case CM_Normal:
                            printf("Data received from %d.\n", i);
                            SU_UpdateMessageCount(i, client_states, MAX_CLIENTS);
                            SU_KickTheDog(i, client_states, MAX_CLIENTS);
                            break;
                    }
                }
            }
        }
        /**
         * Check for connections that need to be closed as we're
         * done processing for now
         */
        close_flagged_connections(client_states);
    }
    return 0;
}
