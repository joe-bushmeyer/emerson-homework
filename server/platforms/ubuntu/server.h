#ifndef __SERVER_H
#define __SERVER_H

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "../../../common/typedefs.h"

/**
 * Listens for client message or times out
 */
ListenResult_t UBU_BlockingListener(signed64_t port, unsigned64_t timeoutSeconds);
/**
 * Tests a specific fd for pending input
 */
boolean_t UBU_IsInputPending(signed64_t fd);
/**
 * Tests to see if we have a new client request
 */
boolean_t UBU_IsNewConnection(signed64_t fd);
/**
 * Accept a new client connection
 */
signed64_t UBU_AcceptConnectRequest(signed64_t fd);
/**
 * Get the client message
 */
unsigned64_t UBU_ReadFromClient(signed64_t fd, char *buffer, unsigned64_t maxLen);
/**
 * Close client connection
 */
void UBU_CloseClientConnection(signed64_t fd);
#endif
