/**
 * server.c
 * Ubuntu server routines
 * 
 */
#include "server.h"

#define UNINITIALIZED_SOCK (-1)

/**
 * State variables used to track status of active sockets
 * across the routines.
 */
static int                  sock = UNINITIALIZED_SOCK;
static fd_set               active_fd_set;
static fd_set               read_fd_set;
static struct timeval       timeout;

int make_socket (uint16_t port)
{
    int new_sock;
    struct sockaddr_in name;

    /* Create the socket. */
    new_sock = socket (PF_INET, SOCK_STREAM, 0);
    if (new_sock < 0)
    {
        perror ("socket");
        exit (EXIT_FAILURE);
    }

    /* Give the socket a name. */
    name.sin_family = AF_INET;
    name.sin_port = htons (port);
    name.sin_addr.s_addr = htonl (INADDR_ANY);
    if (bind (new_sock, (struct sockaddr *) &name, sizeof (name)) < 0)
    {
        perror ("bind");
        exit (EXIT_FAILURE);
    }

    return new_sock;
}

/**
 * Initializes the socket and readies server
 * for connections.
 */
void server_init(signed64_t port) {
    /* Create the socket and set it up to accept connections. */
    sock = make_socket((u_int16_t)port);

    if (listen(sock, 1) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    /* Initialize the set of active sockets. */
    FD_ZERO (&active_fd_set);
    FD_SET  (sock, &active_fd_set);
}

/**
 * Listener
 */
ListenResult_t UBU_BlockingListener(signed64_t port, unsigned64_t timeoutSeconds) {
    ListenResult_t result;

    if (sock == UNINITIALIZED_SOCK) {
        server_init(port);
    }

    // init for listen iteration
    read_fd_set     = active_fd_set;
    timeout.tv_sec  = (__time_t)timeoutSeconds;
    timeout.tv_usec = 0;

    return ((ListenResult_t)
            ((select (FD_SETSIZE, &read_fd_set, NULL, NULL, &timeout))));
}

/**
 * Tests a specific fd for pending input
 */
boolean_t UBU_IsInputPending(signed64_t fd) {
    boolean_t pending_flag = FALSE;

    if (FD_ISSET((int)fd, &read_fd_set)) {
        pending_flag = TRUE;
    }
    return pending_flag;
}

/**
 * Tests to see if we have a new client request
 */
boolean_t UBU_IsNewConnection(signed64_t fd) {
    boolean_t new_flag = FALSE;

    if ((int)fd == sock) {
        new_flag = TRUE;
    }
    return new_flag;
}

/**
 * Accept a new client connection
 */
signed64_t UBU_AcceptConnectRequest(signed64_t fd) {
    int                  new;
    socklen_t            size;
    struct sockaddr_in   clientname;

    size = sizeof(clientname);
    new  = accept((int)fd, (struct sockaddr *) &clientname, &size);

    if (new < 0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    fprintf(stderr, "Server: connect from host %s, port %d.\n\n",
        inet_ntoa(clientname.sin_addr),
        ntohs(clientname.sin_port));
    FD_SET(new, &active_fd_set);

    return new;
}

/**
 * Get the client message
 */
unsigned64_t UBU_ReadFromClient(signed64_t fd, char *buffer, unsigned64_t maxLen) {
    int nbytes = read((int)fd, buffer, maxLen);

    if (nbytes < 0) {
        perror("read");
        exit(EXIT_FAILURE);
    }
    return (unsigned64_t)nbytes;
}

/**
 * Close client connection
 */
void UBU_CloseClientConnection(signed64_t fd) {
    printf("**********Closed %d **********\n", fd);
    shutdown(fd, SHUT_RDWR);
    close((int)fd);
    FD_CLR((int)fd, &active_fd_set);
}
