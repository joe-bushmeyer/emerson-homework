#ifndef __STATE_UTILS_H
#define __STATE_UTILS_H

#include "../common/io_defs.h"
#include "../common/typedefs.h"

/**
 * Initialize array of states to show nothing connected (yet)
 */
void SU_InitClientStates(ClientState_t *states, unsigned64_t numClients);
/**
 * Find an empty "state" variable and start tracking a "new" client connection
 * Returns FALSE if we've hit our limit on clients
 */
boolean_t SU_InitConnectedClientState(signed64_t id, ClientState_t *states, unsigned64_t numClients);
/**
 * Update timeout trackers on connected clients
 */
void SU_UpdateTimeouts(ClientState_t *states, unsigned64_t numClients);
/**
 * Update message count for connected client with matching id
 */
void SU_UpdateMessageCount(signed64_t id, ClientState_t *states, unsigned64_t numClients);
/**
 * Mark client connection for immediate close
 */
void SU_MarkForClosure(signed64_t id, ClientState_t *states, unsigned64_t numClients);
/**
 * Kick the dog on receipt of wd message
 */
void SU_KickTheDog(signed64_t id, ClientState_t *states, unsigned64_t numClients);
#endif