# Emerson Homework

Technical Interview Assignment

## Overview
I tested each executable individually and together on a Virtual Linux machine running Ubuntu. The two applications work together as expected. Their build files are in the server and client folders under the main project directory.

I patterned the code to be independent of platform. Both the client and server projects utilize a "cfg_main.h" header that abstracts the GNU socket functions.

Regarding the variables with module scope in server.c and client.c ... I wouldn't do it this way if I had it to do over again. I should have created a structure containing the variables that I needed (abstracted via macro) and passed a pointer to each of my platform functions from main.

My implementation is valid for this project, but it just feels icky looking back on it.

## Server
My server implementation was patterned (closely) after an "example" server implementation available at https://www.gnu.org/software/libc/manual/html_node/Server-Example.html.

## Client
Like the server, the client code was patterned after snippets found at https://www.binarytides.com/socket-programming-c-linux-tutorial/.

## Common
The contents of the "common" directory are there to tie the two projects together with types, I/O parameters and codec routines.
